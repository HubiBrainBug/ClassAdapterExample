package Controller;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class ControllerBusinessLogicTest {

    ControllerBusinessLogic controllerBL;

    @Before
    public void setUp () throws Exception {
        this.controllerBL = new ControllerBusinessLogic ();
    }

    @Test
    public void getNeuesLand () throws Exception {
        assertEquals (true, controllerBL.addNeuesLand ("Spanien", "Madrid",
                "Euro", "ES"));
    }

    @Test
    public void fileSpeichern () throws Exception {
        assertEquals (false, controllerBL.fileSpeichern ("pdf"));
    }

}