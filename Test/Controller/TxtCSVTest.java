package Controller;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class TxtCSVTest {

    private Export csv;

    @Before
    public void setUp () throws Exception {
        this.csv = new TxtCSV ();
    }

    @Test
    public void getFunktionName () throws Exception {
        assertEquals ("XML", this.csv.getFunktionName ());
    }

    @Test
    public void textSpeichern () throws Exception {
        assertEquals (true, this.csv.textSpeichern ());
    }

}