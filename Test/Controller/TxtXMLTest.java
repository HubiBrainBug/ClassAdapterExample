package Controller;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class TxtXMLTest {
    private Export xml;

    @Before
    public void setUp () throws Exception {
        this.xml = new TxtXML ();
    }
    @Test
    public void getFunktionName () throws Exception {
        assertEquals ("XML", this.xml.getFunktionName ());
    }

    @Test
    public void textSpeichern () throws Exception {
        assertEquals (true, this.xml.textSpeichern ());
    }

}