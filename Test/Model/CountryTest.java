package Model;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class CountryTest {

    Country testCountry;

    @Before
    public void setUp () throws Exception {
       this.testCountry = new Country ("Frankreich", "Paris", "Euro","FR");
    }

    @Test
    public void getLand () throws Exception {
        assertEquals ("Frankreich", testCountry.getLand ());
    }

    @Test
    public void getHauptstadt () throws Exception {
        assertEquals ("Paris", testCountry.getHauptstadt ());
    }

    @Test
    public void getWaehrung () throws Exception {
        assertEquals ("Euro", testCountry.getWaehrung ());
    }

    @Test
    public void getKuerzel () throws Exception {
        assertEquals ("FR", testCountry.getKuerzel ());
    }

}