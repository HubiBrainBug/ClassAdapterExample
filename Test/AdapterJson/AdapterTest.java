package AdapterJson;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class AdapterTest {

    public Adapter testAdapter;

    @Before
    public void setUp () throws Exception {
        this.testAdapter = new Adapter ();
    }

    @Test
    public void getFunktionName () throws Exception {
        assertEquals ("Json", testAdapter.getFunktionName ());
    }

}