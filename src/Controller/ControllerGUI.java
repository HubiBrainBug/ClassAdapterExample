package Controller;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import Model.Country;

/**
 * Klasse ControllerGUI initialisiert alle GUI Controls
 */
public class ControllerGUI{

    // =========Choice Box "Export"==========
    @FXML public ChoiceBox<String> cbExport;

    @FXML private Label exportOk;

    //==========Input Fields=================
    @FXML private TextField tLand;
    @FXML private TextField tHauptstadt;
    @FXML private TextField tWaehrung;
    @FXML private TextField tKuerzel;

    //==========Table========================
    @FXML private TableView<Country> tvTableView;
    @FXML private TableColumn cLand;
    @FXML private TableColumn cHauptstadt;
    @FXML private TableColumn cWaehrung;
    @FXML private TableColumn cKuerzel;

    //==========Länder in Tablle=============
    public static ObservableList<Country> myWorld = FXCollections.observableArrayList ();

    //==========Optionen in Choice Box=======
    public static ObservableList<Export> exportOptions = FXCollections.observableArrayList ();

    //==========Business Logic Controller====
    private ControllerBusinessLogic controllerBL;

    /**
     * Konstruktor instanziert den Business Controller
     */
    public ControllerGUI () {
        this.controllerBL = new ControllerBusinessLogic ();
    }

    /**
     * Es wird der selektierte Punkt der Choicebox ausgewertet und versucht die Daten der Tabelle im selektierten
     * Format zu exportieren bzw. zu speichern
     */
    @FXML
    public void useExportDrag () {
        if (controllerBL.fileSpeichern (cbExport.getValue ())) {
            exportOk.setText ("Export als " + cbExport.getValue () + " ok.");
        } else if (cbExport.getValue ().equals ("Export to...")) {
            exportOk.setText ("");
        } else {
            exportOk.setText ("Export error!");
        }
    }

    /**
     * Wenn Button "Speichern" gedrückt wird, wir ein neues Land in die Tabelle aufgenommen
     */
    @FXML
    public void neuesLandSpeichern () {
        if (controllerBL.addNeuesLand (tLand.getText ().trim (),
                tHauptstadt.getText ().trim (), tWaehrung.getText ().trim (), tKuerzel.getText ().trim ())) {
            tvTableView.refresh ();
            clearInputFields ();
            Alerts.alertErfolgreich ();
        } else {
            Alerts.alertWarnig ();
        }
    }

    /**
     * Methode löscht den Text aus den Input Fields nach dem Speichern
     */
    @FXML
    public void clearInputFields () {
        tLand.clear ();
        tHauptstadt.clear ();
        tWaehrung.clear ();
        tKuerzel.clear ();
    }

    /**
     * Die ChoiceBox für den Export der Tabelle wird befüllt
     */
    @FXML
    public void setCbExport () {
        cbExport.setTooltip (new Tooltip ("Export in verschiedenen Formaten")); //alt Text
        cbExport.getItems ().add ("Export to...");
        cbExport.setValue ("Export to...");

        for (Export e : exportOptions) {
            cbExport.getItems ().add (e.getFunktionName ());
        }
    }

    /**
     * Hilfsmethode um Testländer in die Tabelle zu füllen
     */
    @FXML
    public void setTestLaender () {
        controllerBL.addNeuesLand ("Österreich", "Wien",
                "Euro", "AT");
        controllerBL.addNeuesLand ("Deutschland", "Berlin",
                "Euro", "DE");
    }

    /**
     * Die Tabelle wird erstellt
     */
    @FXML
    public void setTvTableView () {
        tvTableView.setItems (myWorld);
        cLand.setCellValueFactory (new PropertyValueFactory<Country, String> ("land"));
        cHauptstadt.setCellValueFactory (new PropertyValueFactory<Country, String> ("hauptstadt"));
        cWaehrung.setCellValueFactory (new PropertyValueFactory<Country, String> ("waehrung"));
        cKuerzel.setCellValueFactory (new PropertyValueFactory<Country, String> ("kuerzel"));
    }

    /**
     * Die selektierte Zeile wird mit betätigen des Buttons "Löschen" aus der Tabelle gelöscht
     */
    @FXML
    public void deleteItemOfTable () {
        Country selected = tvTableView.getSelectionModel ().getSelectedItem ();
        if (selected != null) {
            myWorld.remove (selected);
            tvTableView.refresh ();
        }else{
            Alerts.alertDelete ();
        }
    }
}
