package Controller;

import javafx.collections.ObservableList;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import Model.Country;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;

/**
 * Klasse TxtXML implementiert das Interface Export
 * bietet eine Methode um die Länder der Tabelle in ein XML-File zu speichern
 */
public class TxtXML implements Export {

    public static final String name = "XML";

    /**
     * @return gibt den Namen der ExportFunktion zurück
     */
    @Override
    public String getFunktionName () { return "XML"; }

    /**
     * Die Methode versucht die Einträge der Tabelle in das XML Format zu exportieren
     * @return true/false ob der Export funktioniert hat
     */
    @Override
    public boolean textSpeichern () {

        try{
            String filepath = "myCountriesInXML.xml";
            DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance ();
            DocumentBuilder docBuilder = docFactory.newDocumentBuilder ();
            Document doc = docBuilder.newDocument ();

            Element root = doc.createElement ("World");
            doc.appendChild (root);

            ObservableList<Country> laender = ControllerGUI.myWorld;

            for(int i = 0; i < laender.size (); i++){
                Element land = doc.createElement ("Land");
                root.appendChild (land);

                Element name = doc.createElement ("Landname");
                name.appendChild (doc.createTextNode (laender.get (i).getLand ()));
                land.appendChild (name);

                Element hauptstadt = doc.createElement ("Hauptstadt");
                hauptstadt.appendChild (doc.createTextNode (laender.get (i).getHauptstadt ()));
                land.appendChild (hauptstadt);

                Element waehrung = doc.createElement ("Waehrung");
                waehrung.appendChild (doc.createTextNode (laender.get (i).getWaehrung ()));
                land.appendChild (waehrung);

                Element kuerzel = doc.createElement ("Kuerzel");
                kuerzel.appendChild (doc.createTextNode (laender.get (i).getKuerzel ()));
                land.appendChild (kuerzel);
            }

            //in XML File schreiben
            TransformerFactory transformerFactory = TransformerFactory.newInstance ();
            Transformer transformer = transformerFactory.newTransformer ();
            DOMSource source = new DOMSource (doc);
            StreamResult result = new StreamResult (new File (filepath));
            transformer.transform (source, result);

            return true;

        } catch (ParserConfigurationException e) {
            e.printStackTrace ();
            return false;
        } catch (TransformerConfigurationException e) {
            e.printStackTrace ();
            return false;
        } catch (TransformerException e) {
            e.printStackTrace ();
            return false;
        }
    }
}
