package Controller;

/**
 * Interface Export implementiert 2 Methoden
 */
public interface Export {

    String getFunktionName ();
    boolean textSpeichern ();
}
