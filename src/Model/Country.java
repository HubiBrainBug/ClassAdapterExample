package Model;

/**
 * Klasse Country ist die Entitätsklasse mit deren Objekte die Tablle befüllt wird
 * es gibt getter
 */
public class Country {  
    private String land;
    private String hauptstadt;
    private String waehrung;
    private String kuerzel;

    public Country (String land, String hauptstadt, String currency, String kuerzel){
        this.land = land;
        this.hauptstadt = hauptstadt;
        this.waehrung = currency;
        this.kuerzel = kuerzel;
    }

    /**
     * @return gibt den Namen des Landes retour
     */
    public String getLand () { return land; }

    /**
     * @return gibt Hauptstadt retour
     */
    public String getHauptstadt () {
        return hauptstadt;
    }

    /**
     * @return gibt Währung retour
     */
    public String getWaehrung () {return waehrung;}

    /**
     * @return gibt Küzel retour
     */
    public String getKuerzel () {return kuerzel;}
}