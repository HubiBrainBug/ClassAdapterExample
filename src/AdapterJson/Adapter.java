package AdapterJson;

import javafx.collections.ObservableList;
import Controller.ControllerGUI;
import Controller.Export;
import Model.Country;

import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Klasse Adapter bietet einen Objektadapter für die Klasse JsonString,
 * welche das Interface Export implementiert.
 */
public class Adapter extends JsonString implements Export {

    private List<Object> alist;
    private String jsonString;

    public Adapter (){
        super();
    }

    /**
     * @return gibt den Namen der ExportFunktion zurück
     */
    @Override
    public String getFunktionName () {return "Json";}

    /**
     * Speichert Json String in ein .txt File
     * @return true/false ob der Export funktioniert hat
     */
    @Override
    public boolean textSpeichern () {
        convert ();

        try (FileWriter file = new FileWriter ("myCountriesToJSON.txt")) {
            file.write (parseToJson ((ArrayList<Object>) alist));
            return true;
        } catch (IOException e) {
            e.printStackTrace ();
        }
        return false;
    }

    /**
     * Konvertiert die Länder in der Tabelle in ein Liste<Object>
     */
    private void convert(){
        this.alist = new ArrayList<Object> ();
        this.alist = ControllerGUI.myWorld.stream ().collect(Collectors.toList());
    }

}
