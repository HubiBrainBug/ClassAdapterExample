package AdapterJson;

import com.google.gson.Gson;

import java.util.ArrayList;

/**
 * Klasse JsonString gibt eine ArrayList<Objects> in Json convertiert als String zurück.
 */
public class JsonString {

    public JsonString () {}

    /**
     * Uebersetzt eine ArrayList in einen Json-String
     * @param listToParse ArrayList<Object> welche in Json übersetzt werden soll
     * @return gibt einen String zurueck welcher die Liste in Json Format enthaelt
     */
    public String parseToJson (ArrayList<Object> listToParse) {

        Gson gson = new Gson ();
        String json = gson.toJson (listToParse);
        System.out.println (json);
        return json;

    }
}