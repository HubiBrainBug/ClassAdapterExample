package sample;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import Controller.*;
import AdapterJson.*;
import Controller.ControllerGUI.*;

public class Main extends Application {
    @Override
    public void start(Stage primaryStage) throws Exception{

        Export csv = new TxtCSV ();
        Export xml = new TxtXML ();
        Export json = new Adapter ();

        ControllerGUI.exportOptions.addAll (csv, xml, json);

        FXMLLoader loader = new FXMLLoader (getClass ().getResource ("../View/sample.fxml"));
        Parent root = loader.load ();

        ControllerGUI c = loader.getController ();

        Scene scene = new Scene (root);
        scene.getStylesheets().add(getClass().getResource("../View/world.css").toExternalForm());

        primaryStage.setTitle ("World Infosystem");
        primaryStage.setScene (scene);
        primaryStage.show ();

        c.setTestLaender ();
        c.setTvTableView ();
        c.setCbExport ();

        c.cbExport.getSelectionModel ()
                .selectedItemProperty ()
                .addListener ((old, oldValue, newValue) -> c.useExportDrag ());
    }


    public static void main(String[] args) {
        launch(args);
    }
}
